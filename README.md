# VoiceFields

## Troubleshooting
If you run into error with the app throwing exceptions and crashing, ensure that you have enabled permissions for the app.

  ``
  Settings->Application manager -> Voice fields -> Permissions -> Microphone
  ``

## Built versions
Built ``apk`` versions located in ``/apk``


