% highpass filter according to dsp cookbook by 
% http://www.musicdsp.org/files/Audio-EQ-Cookbook.txt
% Coefficients taken from http://aikelab.net/filter/ 

disp('Starting');
Fs = 8000;
f0 = 3000;
Q = 2.0;

omega = sn = cs = alpha = 0;

omega = 2 * pi* f0 / 44100;
sn = sin(omega);
cs = cos(omega);
alpha = sn / (2 * Q);

% HPF
b0 = (1 + cs) / 2;
b1 = -(1 + cs);
b2 = (1 + cs) / 2;
a0 = 1 + alpha;
a1 = -2 * cs;
a2 = 1 - alpha;


b0a0 = b0 /a0;
b1a0 = b1 /a0;
b2a0 = b2 /a0;
a1a0 = a1 /a0;
a2a0 = a2 /a0;

x1 = x2 = 0;
y1 = y2 = 0;

% Load sound
%%%%%%%%%%%%%%
inputFile = ('wav/baby-talking_8khz_toned.wav');
inputSignal = audioread(inputFile);

% output 
out = zeros(length(inputSignal), 1);
y = 0;

for i = 1 : 1 : length(inputSignal)
  
  x = inputSignal(i);

  y = b0a0 * x + b1a0 * x1 + b2a0 * x2 - a1a0 * y1 - a2a0 * y2;
 
  x2 = x1;
	x1 = x;
	y2 = y1;
	y1 = y;
  
  out(i) = y;
  
end


%sound(out, Fs);

figure(1);
hold on;
plot(inputSignal, 'b');
plot(out, 'g');
legend('original signal', 'filtered signal');

%sound(inputSignal, Fs);
%sound(out, Fs);