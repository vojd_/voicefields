package se.itchie.voicefields;

public class BandPassFilter extends Filter {
    private final double sampleRate;
    private final double freq;
    private final double Q;

    private double omega;
    private double sn;
    private double cs;
    private double alpha;

    private double a0;
    private double a1;
    private double a2;

    private double b0;
    private double b1;
    private double b2;

    private final double b0a0;
    private final double b1a0;
    private final double b2a0;
    private final double a1a0;
    private final double a2a0;

    private double x1;
    private double x2;
    private double y1;
    private double y2;

    private double y;

    public BandPassFilter(double sampleRate, double freq, double Q) {

        this.sampleRate = sampleRate;
        this.freq = freq;
        this.Q = Q;

        omega = 2 * Math.PI * freq / 44100;
        sn = Math.sin(omega);
        cs = Math.cos(omega);
        alpha = sn / (2 * Q);

        b0 =   Math.sin(omega)/2; //  =   Q*alpha
        b1 =   0;
        b2 =  -Math.sin(omega)/2; // =  -Q*alpha
        a0 =   1 + alpha;
        a1 =  -2*Math.cos(omega);
        a2 =   1 - alpha;


        b0a0 = b0 /a0;
        b1a0 = b1 /a0;
        b2a0 = b2 /a0;
        a1a0 = a1 /a0;
        a2a0 = a2 /a0;

        x1 = x2 = 0;
        y1 = y2 = 0;
    }

    @Override
    public double process(double x) {
        y = b0a0 * x + b1a0 * x1 + b2a0 * x2 - a1a0 * y1 - a2a0 * y2;

        x2 = x1;
        x1 = x;
        y2 = y1;
        y1 = y;
        return y;
    }
}
