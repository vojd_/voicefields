package se.itchie.voicefields;


public abstract class Filter {

    public double process(double x) {
        return 0;
    }

    public void processBuffer(double[] out, double[] in) {
        for(int i=0; i<in.length; i++) {
            out[i] = this.process(in[i]);
        }
    }
}
