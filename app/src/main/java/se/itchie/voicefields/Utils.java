package se.itchie.voicefields;

public class Utils {

    // Pre-define everything we need to avoid heap allocations
    double rmsSum = 0.0f;
    double nofCrossings = 0;
    public double calculateRMS(double[] buffer) {
        rmsSum = 0.0f;
        for(double n : buffer) {
            rmsSum += n * n;
        }
        return Math.sqrt(rmsSum / buffer.length);
    }

    public double calculateZCR(double[] buffer) {
        nofCrossings = 0.0f;
        for(int i=1; i<buffer.length; i++) {
            if(buffer[i]*buffer[i-1] > 0) {
                nofCrossings++;
            }
        }
        return nofCrossings / buffer.length;
    }

    public static void shortsToDoubles(double[] out, short[] in) {
        for(int i=0; i<in.length; i++) {
            out[i] = (double) in[i];
        }
    }

    public static void doublesToShorts(short[] out, double[] in) {
        for(int i=0; i<in.length; i++) {
            out[i] = (short) in[i];
        }
    }

    public static double doubleSum(double[] arr) {
        double sum = 0.0;
        for(double n : arr) {
            sum += n;
        }
        return sum;
    }
}
