package se.itchie.voicefields;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import se.itchie.voicefields.renderers.GridRenderer;


class MyGLSurfaceView extends GLSurfaceView implements ErrorHandler {

    private GridRenderer renderer;
    private float density;

    // Offsets for touch events
    private float previousX;
    private float previousY;


    private final VoiceActivityDetector detector;
    private final Audio audio;

    private final int SAMPLE_RATE = 8000;

    public MyGLSurfaceView(Context context) {
        super(context);

        // detector can be regarded as the model, contains the data
        detector = new VoiceActivityDetector(SAMPLE_RATE);

        audio = new Audio(SAMPLE_RATE, detector, this);
        audio.start();

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);
    }

    public void notifyAudioWasUpdated() {
        try {
            queueEvent(new Runnable() {
                @Override
                public void run() {
                    renderer.setState(detector.getState());
                }
            });
        }catch (Exception e){
            Log.e("Runnable", "" + e.getMessage());
        }
    }

    private void notifyThresholdChanged() {
        try {
            queueEvent(new Runnable() {
                @Override
                public void run() {
                    double[] state = detector.getState();
                    state[1] += dirY;
                    detector.setState(state);
                }
            });
        }catch (Exception e) {
            Log.e("Runnable", "" + e.getMessage());
        }

    }

    int y = 0;
    int prevY = 0;
    int dirY = 0;

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        super.dispatchTouchEvent(event);

        switch (event.getAction())
        {
            case MotionEvent.ACTION_MOVE:
                y = (int) event.getRawY();
                dirY = prevY < y ? -1 : 1;
                notifyThresholdChanged();
                break;

            case MotionEvent.ACTION_UP:
                prevY = (int) event.getRawY();
                dirY = 0;
                break;

            case MotionEvent.ACTION_DOWN:
                prevY = (int) event.getRawY();
                y = (int) event.getRawY();
                break;
        }
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event != null)
        {
            float x = event.getX();
            float y = event.getY();

            if (event.getAction() == MotionEvent.ACTION_MOVE)
            {
                if (renderer != null)
                {
                    float deltaX = (x - previousX) / density / 2f;
                    float deltaY = (y - previousY) / density / 2f;

                    renderer.deltaX += deltaX;
                    renderer.deltaY += deltaY;
                }
            }

            previousX = x;
            previousY = y;

            return true;
        }
        else
        {
            return super.onTouchEvent(event);
        }
    }
    @Override
    public void handleError(final ErrorType errorType, final String cause) {
        // Queue on UI thread.
        post(new Runnable() {
            @Override
            public void run() {
                final String text;

                switch (errorType) {
                    case BUFFER_CREATION_ERROR:
                        /*
                        text = String
                                .format(getContext().getResources().getString(
                                        R.string.lesson_eight_error_could_not_create_vbo), cause);
                        */
                        text = "Buffer creation error";
                        break;
                    default:
                        text = "Default error";
                }

                Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();

            }
        });
    }

    public void setRenderer(GridRenderer renderer, float density) {

        this.renderer = renderer;
        this.density = density;
        super.setRenderer(renderer);
    }
}
