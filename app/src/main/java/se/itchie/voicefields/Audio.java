package se.itchie.voicefields;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.util.Log;

public class Audio extends Thread {

    private final MyGLSurfaceView view;
    private final int SAMPLE_RATE;
    private AudioRecord recorder;
    private AudioTrack track;
    private VoiceActivityDetector detector;

    private boolean stopped = false;
    private short[] buffer = new short[160];

    public Audio(int SAMPLE_RATE, VoiceActivityDetector detector, MyGLSurfaceView view) {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        this.SAMPLE_RATE = SAMPLE_RATE;
        this.detector = detector;
        this.view = view;
    }

    @Override
    public void run() {
        Log.i("Audio", "Running Audio Thread");
        recorder = null;
        track = null;

        try {
            int N = AudioRecord.getMinBufferSize(
                    SAMPLE_RATE,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT); // Big endian signed

            recorder = new AudioRecord(
                    MediaRecorder.AudioSource.MIC,
                    SAMPLE_RATE,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, // Big endian signed
                    N*10);

            track = new AudioTrack(
                    AudioManager.STREAM_MUSIC,
                    SAMPLE_RATE,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    N,
                    AudioTrack.MODE_STREAM);

            recorder.startRecording();

            track.setPlaybackRate(SAMPLE_RATE);

            double[] dblBuffer = new double[buffer.length];

            while(!stopped) {
                recorder.read(buffer, 0, buffer.length);
                Utils.shortsToDoubles(dblBuffer, buffer);
//                filter.processBuffer(filteredBuffer, dblBuffer);
                detector.update(dblBuffer);

                view.notifyAudioWasUpdated();

                // TODO: Add GUI for toggling playback of buffer
                // playbackBuffer(filteredBuffer);
            }
        } catch(Throwable x) {
            Log.w("Audio", "Error reading voice audio", x);
        } finally {
            close();
        }
    }

    private void playbackBuffer(double[] buffer) {
        // Playback sound, can be heard through the speakers. Used for reference listening
        track.play();
        // Got to convert the doubles back to shorts to be able to play the sound
        short[] shortsBuffer = new short[buffer.length];
        Utils.doublesToShorts(shortsBuffer, buffer);
        track.write(shortsBuffer, 0, buffer.length);
    }

    /**
     * Called from outside of the thread in order to stop the recording/playback loop
     */
    public void close() {
        stopped = true;
        Log.v("Audio", "Audio closed: " + recorder);
        if(recorder != null) {
            recorder.stop();
            recorder.release();
            Log.v("Audio", "Stopped and released audio recorder");
        }
    }
}
