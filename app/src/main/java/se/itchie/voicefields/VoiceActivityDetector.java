package se.itchie.voicefields;

import android.util.Log;

import java.util.stream.DoubleStream;

import se.itchie.voicefields.processors.AudioProcess;

public class VoiceActivityDetector extends AudioProcess {

    private Utils util = new Utils();

    public static final double MAX_THRESHOLD = 32f;
    public static final double MIN_THRESHOLD = 6f;
    private double energyLevel = 1;
    private double threshold = MIN_THRESHOLD;
    private double thresholdScale = 20.0;

    private double zcrThreshold = 0.1f;

    private double fVAD = 0; //  voice detected, 0=false, 1=true
    private boolean hVAD = false; //  holdover

    private double zcr;

    Filter filter;

    public VoiceActivityDetector(int SAMPLE_RATE) {
        filter = new HighPassFilter(SAMPLE_RATE, 3000, 2.0);
    }

    public void update(double[] buffer) {

        fVAD = 0; // Could be a boolean but we don't want to cast between int and bool all the time

        zcr = util.calculateZCR(buffer);

        filter.processBuffer(buffer, buffer); // TODO: Check if we get errors when in and out buffers are the same

        // Max recorded rms value: 14611
        // Setting a limit on 14000
        double rms = Math.min(14000, util.calculateRMS(buffer));

        // Scale the energy level to fit the threshold and graphical visualization
        energyLevel = Math.min(MAX_THRESHOLD, (rms * 2.3) / 250.0);

        double sum = Utils.doubleSum(buffer);
        threshold = Math.abs((1.0 / buffer.length) * sum);
        threshold = threshold * thresholdScale;

        if(energyLevel > threshold * 2 && zcr > zcrThreshold) {
            fVAD = 1;    // Voice is detected
            hVAD = true; // Keep VAD active next frame
        } else {
            if(hVAD) {
                hVAD = false;
                fVAD = 1;
            } else {
              fVAD = 0;
            }
        }

        if(fVAD != 1) {
            threshold = 0.95 * threshold + 0.05 * energyLevel;

        }
        Log.v("update", "e: " + energyLevel + " t: " + threshold + " zcr: " + zcr + " : " + sum);

    }

    public double[] getState() {
        return new double[]{energyLevel, threshold, fVAD};
    }

    public void setState(double [] state) {
        double t = state[1];
        if(t > MAX_THRESHOLD) t = MAX_THRESHOLD;
        if(t <= MIN_THRESHOLD) t = MIN_THRESHOLD;

        energyLevel = state[0];
        threshold = t;
        fVAD = state[2];
    }
}
