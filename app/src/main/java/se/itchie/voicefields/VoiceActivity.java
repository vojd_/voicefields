package se.itchie.voicefields;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import se.itchie.voicefields.renderers.GridRenderer;

public class VoiceActivity extends AppCompatActivity {

    MyGLSurfaceView glView;
    GridRenderer renderer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        // Here's the view. Presentation layer for the detector model.
        // Can be seen as the controller. Pulls data from detector into renderer.
        glView = new MyGLSurfaceView(this);
        setContentView(glView);

        glView.setEGLContextClientVersion(2);

        renderer = new GridRenderer(this);
        glView.setRenderer(renderer, displayMetrics.density);
    }
}
