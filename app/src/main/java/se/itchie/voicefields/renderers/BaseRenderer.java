package se.itchie.voicefields.renderers;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class BaseRenderer implements GLSurfaceView.Renderer {

    private static final String TAG = "Renderer";

    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    // Model = transforms vertices in model space to the world space
    protected float[] mProjectionMatrix = new float[16];
    protected float[] mViewMatrix = new float[16];
    protected float[] scratch = new float[16];
    protected float[] mMVPMatrix = new float[16];

    // [0] = energy
    protected double[] state;

    protected int muMVPMatrixHandle;
    protected int mProgram; // Shader program

    public BaseRenderer() {

    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
    }

    public void onDrawFrame(GL10 unused) {
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
    }

    /**
     *
     * @param type
     * @param shaderCode
     * @return shader
     * @deprecated Use GfxUtils instead
     */
    @Deprecated
    public static int loadShader(int type, String shaderCode){

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    /**
     *
     * @param glOperation
     * @deprecated Use GfxUtils instead
     */
    @Deprecated
    public static void checkGlError(String glOperation) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, glOperation + ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }

    public void setState(double[] state) {
        this.state = state;
    }

    public double[] getState() {
        return this.state;
    }
}
