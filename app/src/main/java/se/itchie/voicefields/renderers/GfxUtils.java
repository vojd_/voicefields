package se.itchie.voicefields.renderers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.opengl.GLUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public class GfxUtils {

    public static void checkGlError(String TAG, String glOperation) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, glOperation + ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }

    public static int loadShader(int type, String shaderCode) {

        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    public static int loadTexture(final Context context, final int resourceId) {

        final int[] textureHandle = new int[1];

        // Generate texture handle
        GLES20.glGenTextures(1, textureHandle, 0);

        if(textureHandle[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false; // No pre-scaling

            // Create bitmap from image
            final Bitmap bitmap = BitmapFactory
                    .decodeResource(context.getResources(), resourceId, options);

            // Bind to the texture in OpenGL
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            // Set filtering
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

            // Load bitmap into bound texture
            GLUtils.texImage2D(
                    GLES20.GL_TEXTURE_2D, // type
                    0,                    // level
                    bitmap,               // bitmap
                    0                     // border
            );

            bitmap.recycle();
        }

        if(textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture");
        }

        return textureHandle[0];
    }

    public static int createAndBindEmptyTexture(float[] textureData, int dimensions) {

        // Generate textureData

        int[] textureHandle = new int[1];

        int offset = 0;

        // Generate texture textureData
        for(int y = 0; y < dimensions; y++) {
            for(int x = 0; x < dimensions; x++) {
                try {
                    textureData[offset++] = 1.0f / x;
                    textureData[offset++] = 0.2f / x;
                    textureData[offset++] = 0.2f / x;
                    textureData[offset++] = 1.0f;
                }catch (ArrayIndexOutOfBoundsException e ) {
                    Log.e("Error", " out " + e.getMessage());
                }
            }
        }

        //
        final FloatBuffer buffer = ByteBuffer
                .allocateDirect(textureData.length * 4) // float byte length = 4
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        buffer.put(textureData, 0, textureData.length).position(0);

        // Bind textureData to texture

        GLES20.glGenTextures(1, textureHandle, 0);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

        Log.v("Create texture", "Buffer done" );
        GLES20.glTexImage2D(
                GLES20.GL_TEXTURE_2D,
                0,
                GLES20.GL_RGBA,
                dimensions,
                dimensions,
                0,
                GLES20.GL_RGBA,
                GLES20.GL_FLOAT,
                buffer);

        Log.v("Create texture", "GL done");
        return textureHandle[0];
    }

    /**
     * Moves the whole array "left" and inserts a new column to the right
     * @param textureData
     * @param dimensions
     * @param textureHandle
     * @param amplitude
     */
    public static void updateTexture(float[] textureData, int dimensions, int textureHandle, float amplitude) {
        if(textureData == null) {
            return;
        }

        int offset = 0;

        // Move contents to the left, one

        int px;

        int colorLength = 4;
        int width = dimensions * colorLength;
        int height = dimensions;

        // Scroll everything left
        for(int y=0; y<height; y++) {
            for(int x=colorLength; x<width; x+=colorLength) {
                px = x - colorLength;
                textureData[px + 	 (y * width)] = textureData[x +     (y * width)];
                textureData[px + 1 + (y * width)] = textureData[x + 1 + (y * width)];
                textureData[px + 2 + (y * width)] = textureData[x + 2 + (y * width)];
                textureData[px + 3 + (y * width)] = 1.0f;
            }
        }

        int yu, ym, yd;
        float amp;
        // Add new column to the right of array
        for(int y=1; y<height/2; y++) {
            for(int x=width-colorLength; x<width; x+=colorLength) {

                yu = height/2 - y;
                yd = height/2 + y;

                amp = 1.0f/y + amplitude;
                // top half
                textureData[x +     (yu * width)] = amp;
                textureData[x + 1 + (yu * width)] = amp;
                textureData[x + 2 + (yu * width)] = amp;
                textureData[x + 3 + (yu * width)] = amp;

                // bottom half
                textureData[x +     (yd * width)] = amp;
                textureData[x + 1 + (yd * width)] = amp;
                textureData[x + 2 + (yd * width)] = amp;
                textureData[x + 3 + (yd * width)] = amp;
            }
        }

        // insert middle

        // end of middle
        ym = height/2;
        textureData[width-colorLength +     (ym * width)] = amplitude;
        textureData[width-colorLength + 1 + (ym * width)] = amplitude;
        textureData[width-colorLength + 2 + (ym * width)] = amplitude;
        textureData[width-colorLength + 3 + (ym * width)] = amplitude;


        final FloatBuffer buffer = ByteBuffer
                .allocateDirect(textureData.length * 4) // float byte length = 4
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        buffer.put(textureData, 0, textureData.length).position(0);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

        GLES20.glTexImage2D(
                GLES20.GL_TEXTURE_2D,
                0,
                GLES20.GL_RGBA,
                dimensions,
                dimensions,
                0,
                GLES20.GL_RGBA,
                GLES20.GL_FLOAT,
                buffer);
    }
}
