package se.itchie.voicefields.renderers;

import android.opengl.GLES20;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import se.itchie.voicefields.renderers.EnergySlabRenderer;

public class SlabShape {

    private FloatBuffer vertexBuffer;

    private final float[] mMVPMatrix = new float[16];
    private final float[] mRotationMatrix = new float[16];
    float[] scratch = new float[16]; // 4x4 matrix
    float transY; // translate y

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;

    private final String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
            "attribute vec4 vPosition;" +
            "void main() {" +
                "  gl_Position = uMVPMatrix * vPosition;" +
            "}";

    // TODO: Add phong and bloom/glow shader
    private final String fragmentShaderCode =
            "precision mediump float;" +
            "uniform vec4 vColor;" +
            "void main() {" +
                "  gl_FragColor = vColor;" +
            "}";

    private int mMVPMatrixHandle;

    private final int mProgram;
    private int mPositionHandle;
    private int mColorHandle;

    private final int vertexCount;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    private float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };

    private float[] shapeCoords;

    public SlabShape(float size, float[] color) {

        shapeCoords = this.createSlab(size);

        // initialize vertex byte buffer for shape coordinates
        // (number of coordinate values * 4 bytes per float)
        ByteBuffer bb = ByteBuffer.allocateDirect(shapeCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());
        vertexCount = shapeCoords.length / COORDS_PER_VERTEX;
        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put(shapeCoords);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0);

        int vertexShader = EnergySlabRenderer.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = EnergySlabRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLES20.glCreateProgram();

        // Add shaders
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);

        GLES20.glLinkProgram(mProgram);

        // Set color with red, green, blue and alpha (opacity) values
        this.color = color;

    }

    private float[] createSlab(float s) {
        float h = 0.1f; // TODO: Make configurable
        return new float[]{
                -s,-h,-s, // triangle 1 : begin
                -s,-h, s,
                -s, h, s, // triangle 1 : end

                 s, h,-s, // triangle 2 : begin
                -s,-h,-s,
                -s, h,-s, // triangle 2 : end

                 s,-h, s,
                -s,-h,-s,
                 s,-h,-s,

                 s, h,-s,
                 s,-h,-s,
                -s,-h,-s,

                -s,-h,-s,
                -s, h, s,
                -s, h,-s,

                 s,-h, s,
                -s,-h, s,
                -s,-h,-s,

                -s, h, s,
                -s,-h, s,
                 s,-h, s,

                s, h, s,
                s,-h,-s,
                s, h,-s,

                s,-h,-s,
                s, h, s,
                s,-h, s,

                s, h, s,
                s, h,-s,
                -s, h,-s,

                s, h, s,
                -s, h,-s,
                -s, h, s,

                s, h, s,
                -s, h, s,
                s,-h, s
        };
    }

    public float[] update(double idx, float[] mViewMatrix, float[] mProjectionMatrix) {

        Matrix.setIdentityM(scratch, 0);

        Matrix.setRotateM(mRotationMatrix,
                0,   // offset
                45f, // angle
                -1f, // x
                1f,  // y
                0f   // z
        );

        // Translate
        transY = (float) (idx * 0.21f);
        Matrix.translateM(mRotationMatrix, 0, 0, transY, 0f);

        Matrix.multiplyMM(scratch, 0, mMVPMatrix, 0, mRotationMatrix, 0);

        // Projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        return scratch;

    }

    /**
     * TODO: Use one draw function which accepts all objects instead of this OOP crap
     * @param mvpMatrix
     */
    public void draw(float[] mvpMatrix) {

        GLES20.glUseProgram(mProgram);

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
                mPositionHandle,
                COORDS_PER_VERTEX,
                GLES20.GL_FLOAT,
                false,
                vertexStride,
                vertexBuffer);

        // get handle to fragment shader's vColor member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        // Set color for drawing the triangle
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);

        // handle to transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // pass projection and view transformation to shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

        // Draw the slab
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}