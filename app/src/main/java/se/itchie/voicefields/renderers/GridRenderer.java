package se.itchie.voicefields.renderers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import se.itchie.voicefields.R;
import se.itchie.voicefields.VoiceActivity;
import se.itchie.voicefields.common.RawResourceReader;
import se.itchie.voicefields.common.ShaderHelper;

/**
 * This class implements our custom renderer. Note that the GL10 parameter
 * passed in is unused for OpenGL ES 2.0 renderers -- the static class GLES20 is
 * used instead.
 */
public class GridRenderer extends BaseRenderer {

    private static final String TAG = "GridRenderer";

    private final Activity activity;

    /**
     * Store the model matrix. This matrix is used to move models from object
     * space (where each model can be thought of being located at the center of
     * the universe) to world space.
     */
    private final float[] modelMatrix = new float[16];

    /**
     * Store the view matrix. This can be thought of as our camera. This matrix
     * transforms world space to eye space; it positions things relative to our
     * eye.
     */
    private final float[] viewMatrix = new float[16];

    /**
     * Store the projection matrix. This is used to project the scene onto a 2D
     * viewport.
     */
    private final float[] projectionMatrix = new float[16];

    /**
     * Allocate storage for the final combined matrix. This will be passed into
     * the shader program.
     */
    private final float[] mvpMatrix = new float[16];

    /**
     * Additional matrices.
     */
    private final float[] accumulatedRotation = new float[16];
    private final float[] currentRotation = new float[16];
    private final float[] lightModelMatrix = new float[16];
    private final float[] temporaryMatrix = new float[16];

    /**
     * OpenGL handles to our program uniforms.
     */
    private int mvpMatrixUniform;
    private int mvMatrixUniform;
    private int lightPosUniform;

    /**
     * OpenGL handles to our program attributes.
     */
    private int positionAttribute;
    private int normalAttribute;
    private int colorAttribute;
    private int textureCoordinateAttribute;

    /**
     * Identifiers for our uniforms and attributes inside the shaders.
     */
    private static final String MVP_MATRIX_UNIFORM = "u_MVPMatrix";
    private static final String MV_MATRIX_UNIFORM = "u_MVMatrix";
    private static final String LIGHT_POSITION_UNIFORM = "u_LightPos";
    private static final String TEXTURE_UNIFORM = "u_Texture";

    private static final String POSITION_ATTRIBUTE = "a_Position";
    private static final String NORMAL_ATTRIBUTE = "a_Normal";
    private static final String COLOR_ATTRIBUTE = "a_Color";
    private static final String TEX_COORDINATE_ATTRIBUTE = "a_TexCoordinate";

    /**
     * Additional constants.
     */
    private static final int NOF_POS_ELEMENTS = 3;
    private static final int NOF_NORMAL_ELEMENTS = 3;
    private static final int NOF_COLOR_ELEMENTS = 4;
    private static final int NOF_TEXTURE_COORD_ELEMENTS = 2;
    private static final int BYTES_PER_FLOAT = 4;
    private static final int BYTES_PER_SHORT = 2;
    private static final int STRIDE = (NOF_POS_ELEMENTS + NOF_NORMAL_ELEMENTS + NOF_COLOR_ELEMENTS
            + NOF_TEXTURE_COORD_ELEMENTS) * BYTES_PER_FLOAT;
    private static final int TEXTURE_DIMENSIONS = 32;

    /**
     * Used to hold a light centered on the origin in model space. We need a 4th
     * coordinate so we can get translations to work when we multiply this by
     * our transformation matrices.
     */
    private final float[] lightPosInModelSpace = new float[]{0.0f, 0.0f, 0.0f, 1.0f};

    /**
     * Used to hold the current position of the light in world space (after
     * transformation via model matrix).
     */
    private final float[] lightPosInWorldSpace = new float[4];

    /**
     * Used to hold the transformed position of the light in eye space (after
     * transformation via modelview matrix)
     */
    private final float[] lightPosInEyeSpace = new float[4];

    /**
     * This is a handle to our cube shading program.
     */
    private int program;

    /**
     * Retain the most recent delta for touch events.
     */
    // These still work without volatile, but refreshes are not guaranteed to
    // happen.
    public volatile float deltaX;
    public volatile float deltaY;

    /**
     * The current heightmap object.
     */
    private HeightMap heightMap;
    private int textureDataHandle;
    private float[] textureData = new float[TEXTURE_DIMENSIONS * TEXTURE_DIMENSIONS * 4]; // 3 = RGBA;

    private long endTime;
    private long startTime;
    private long dt;
    private float amplitude;

    // Background color
    private float backgroundR;
    private float backgroundG;
    private float backgroundB;
    private float[] orgBgColor;

    /**
     * Initialize the model data.
     */
    public GridRenderer(final VoiceActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {

        resetBackgroundColor();
        orgBgColor = new float[]{backgroundR, backgroundG, backgroundB};
        heightMap = new HeightMap();

        // Set the background clear color to black.
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // Enable depth testing
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        // Position the eye in front of the origin.
        final float eyeX = 0.0f;
        final float eyeY = 0.0f;
        final float eyeZ = -0.5f;

        // We are looking toward the distance
        final float lookX = 0.0f;
        final float lookY = 0.0f;
        final float lookZ = -5.0f;

        // Set our up vector. This is where our head would be pointing were we
        // holding the camera.
        final float upX = 0.0f;
        final float upY = 1.0f;
        final float upZ = 0.0f;

        // Set the view matrix. This matrix can be said to represent the camera
        // position.
        // NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination
        // of a model and view matrix. In OpenGL 2, we can keep track of these
        // matrices separately if we choose.
        Matrix.setLookAtM(viewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

        final String vertexShader = RawResourceReader
                .readTextFileFromRawResource(activity, R.raw.per_pixel_heightmap_vertex_shader);
        final String fragmentShader = RawResourceReader
                .readTextFileFromRawResource(activity, R.raw.per_pixel_frag_shader);

        final int vertexShaderHandle = ShaderHelper
                .compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);
        final int fragmentShaderHandle = ShaderHelper
                .compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);

        String[] attributes = new String[]
                {POSITION_ATTRIBUTE, NORMAL_ATTRIBUTE, COLOR_ATTRIBUTE, TEX_COORDINATE_ATTRIBUTE};
        program = ShaderHelper
                .createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle, attributes);

        // Initialize the accumulated rotation matrix
        Matrix.setIdentityM(accumulatedRotation, 0);

//        textureDataHandle = GfxUtils.loadTexture(activity, R.drawable.waves);

        textureDataHandle = GfxUtils.createAndBindEmptyTexture(textureData, TEXTURE_DIMENSIONS);
    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to the same size as the surface.
        GLES20.glViewport(0, 0, width, height);

        // Create a new perspective projection matrix. The height will stay the
        // same while the width will vary as per aspect ratio.
        final float ratio = (float) width / height;
        final float left = -ratio;
        final float right = ratio;
        final float bottom = -1.0f;
        final float top = 1.0f;
        final float near = 1.0f;
        final float far = 1000.0f;

        Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {

        // TODO: Timer based rendering
        endTime = System.currentTimeMillis();
        dt = endTime - startTime;
        if (dt < 33){
            try {
                Thread.sleep(33 - dt); // 33 - dt
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        startTime = System.currentTimeMillis();

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        // Is voice detected?
        if(state != null && state[2] == 1) {
            backgroundR = 0.31764f;
            backgroundG = 0.56254f;
            backgroundB = 0.31764f;
        } else {
            interpolateBackgroundColor();
        }

        GLES20.glClearColor(backgroundR, backgroundG, backgroundB, 1f);

        GLES20.glUseProgram(program);

        // Shader handles
        mvpMatrixUniform = GLES20.glGetUniformLocation(program, MVP_MATRIX_UNIFORM);
        mvMatrixUniform = GLES20.glGetUniformLocation(program, MV_MATRIX_UNIFORM);
        lightPosUniform = GLES20.glGetUniformLocation(program, LIGHT_POSITION_UNIFORM);

        positionAttribute = GLES20.glGetAttribLocation(program, POSITION_ATTRIBUTE);
        normalAttribute = GLES20.glGetAttribLocation(program, NORMAL_ATTRIBUTE);
        colorAttribute = GLES20.glGetAttribLocation(program, COLOR_ATTRIBUTE);
        textureCoordinateAttribute = GLES20.glGetAttribLocation(program, TEX_COORDINATE_ATTRIBUTE);

        // Calculate position of the light. Push into the distance.
        Matrix.setIdentityM(lightModelMatrix, 0);
        Matrix.translateM(lightModelMatrix, 0, 0.0f, 7.5f, -8.0f);

        Matrix.multiplyMV(lightPosInWorldSpace, 0, lightModelMatrix, 0, lightPosInModelSpace, 0);
        Matrix.multiplyMV(lightPosInEyeSpace, 0, viewMatrix, 0, lightPosInWorldSpace, 0);

        // Draw the heightmap.
        // Translate the heightmap into the screen.
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -12f);

        // Set a matrix that contains the current rotation.
        Matrix.setIdentityM(currentRotation, 0);
        Matrix.rotateM(currentRotation, 0, deltaX, 0.0f, 1.0f, 0.0f);
        Matrix.rotateM(currentRotation, 0, deltaY, 1.0f, 0.0f, 0.0f);
        deltaX = 0.0f;
        deltaY = 0.0f;

        // Multiply the current rotation by the accumulated rotation, and then
        // set the accumulated rotation to the result.
        Matrix.multiplyMM(temporaryMatrix, 0, currentRotation, 0, accumulatedRotation, 0);
        System.arraycopy(temporaryMatrix, 0, accumulatedRotation, 0, 16);

        // Rotate the cube taking the overall rotation into account.
        Matrix.multiplyMM(temporaryMatrix, 0, modelMatrix, 0, accumulatedRotation, 0);
        System.arraycopy(temporaryMatrix, 0, modelMatrix, 0, 16);

        // This multiplies the view matrix by the model matrix, and stores
        // the result in the MVP matrix
        // (which currently contains model * view).
        Matrix.multiplyMM(mvpMatrix, 0, viewMatrix, 0, modelMatrix, 0);

        // Pass in the modelview matrix.
        GLES20.glUniformMatrix4fv(mvMatrixUniform, 1, false, mvpMatrix, 0);

        // This multiplies the modelview matrix by the projection matrix,
        // and stores the result in the MVP matrix
        // (which now contains model * view * projection).
        Matrix.multiplyMM(temporaryMatrix, 0, projectionMatrix, 0, mvpMatrix, 0);
        System.arraycopy(temporaryMatrix, 0, mvpMatrix, 0, 16);

        // Pass in the combined matrix.
        GLES20.glUniformMatrix4fv(mvpMatrixUniform, 1, false, mvpMatrix, 0);

        // Pass in the light position in eye space.
        GLES20.glUniform3f(
                lightPosUniform,
                lightPosInEyeSpace[0],
                lightPosInEyeSpace[1],
                lightPosInEyeSpace[2]
        );


        //////////////
        // Textures //
        //////////////
        // Activate texture unit = 0
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        // Bind texture from handle to this texture unit
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureDataHandle);

        double[] state = this.getState();
        if(state == null){
            amplitude = 0.0f;
        }else{
            amplitude = (float) (this.getState()[0]);
        }

        GfxUtils.updateTexture(textureData, TEXTURE_DIMENSIONS, textureDataHandle, amplitude);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_MIRRORED_REPEAT);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_MIRRORED_REPEAT);

        // Texture uniform sampler can use the texture bound in texture unit 0
        GLES20.glUniform1i(textureDataHandle, 0);

        // Render the heightmap.
        heightMap.render();
    }

    private void interpolateBackgroundColor() {

        if(backgroundR > orgBgColor[0] ) {
            backgroundR -= 0.004;
        }

        if(backgroundG > orgBgColor[1]) {
            backgroundG -= 0.004;
        }

        if(backgroundB < orgBgColor[2]) {
            backgroundB += 0.004;
        }
    }

    private void resetBackgroundColor() {
        backgroundR = 0.239f;
        backgroundG = 0.21568f;
        backgroundB = 0.560784f;
    }

    class HeightMap {
        static final int SIZE_PER_SIDE = 32;
        static final float MIN_POSITION = -5f;
        static final float POSITION_RANGE = 10f;

        final int[] vbo = new int[1];
        final int[] ibo = new int[1];

        int indexCount;

        FloatBuffer textureCoordinates;
        float[] textureCoordinateData;

        HeightMap() {
            try {
                final int floatsPerVertex = NOF_POS_ELEMENTS + NOF_NORMAL_ELEMENTS + NOF_COLOR_ELEMENTS + NOF_TEXTURE_COORD_ELEMENTS;
                final int xLength = SIZE_PER_SIDE;
                final int yLength = SIZE_PER_SIDE;

                //[ XYZ NNN RGBA ST ]
                final float[] vboData = new float[xLength * yLength * floatsPerVertex];

                textureCoordinateData = new float[xLength * yLength * 3];

                int offset = 0;

                float texCoordX = 0.0f;
                float texCoordY = 0.0f;

                float txStep = 1.0f / SIZE_PER_SIDE;

                // First, build the data for the vertex buffer
                for (int y = 0; y < yLength; y++) {
                    for (int x = 0; x < xLength; x++) {
                        final float xRatio = x / (float) (xLength - 1);

                        // Build our heightmap from the top down, so that our triangles are counter-clockwise.
                        final float yRatio = 1f - (y / (float) (yLength - 1));

                        // -5.0 to 5.0
                        final float xPosition = MIN_POSITION + (xRatio * POSITION_RANGE);
                        final float yPosition = MIN_POSITION + (yRatio * POSITION_RANGE);

                        // X Y Z
                        vboData[offset++] = xPosition;
                        vboData[offset++] = yPosition;
                        vboData[offset++] = 0.0f;

                        // Cheap normal using a derivative of the function.
                        // The slope for X will be 2X, for Y will be 2Y.
                        // Divide by 10 since the position's Z is also divided by 10.
                        // TODO: Recalculate normal in vertex shader after deforming the height map
                        final float xSlope = (2 * xPosition) / 10f;
                        final float ySlope = (2 * yPosition) / 10f;

                        // Calculate the normal using the cross product of the slopes.
                        final float[] planeVectorX = {1f, 0f, xSlope};
                        final float[] planeVectorY = {0f, 1f, ySlope};
                        final float[] normalVector = {
                                (planeVectorX[1] * planeVectorY[2]) - (planeVectorX[2] * planeVectorY[1]),
                                (planeVectorX[2] * planeVectorY[0]) - (planeVectorX[0] * planeVectorY[2]),
                                (planeVectorX[0] * planeVectorY[1]) - (planeVectorX[1] * planeVectorY[0])
                        };

                        // Normalize the normal
                        final float length = Matrix.length(normalVector[0], normalVector[1], normalVector[2]);

                        vboData[offset++] = normalVector[0] / length;
                        vboData[offset++] = normalVector[1] / length;
                        vboData[offset++] = normalVector[2] / length;

                        // R G B A
                        vboData[offset++] = xRatio;
                        vboData[offset++] = yRatio;
                        vboData[offset++] = 0.5f;
                        vboData[offset++] = 1f;

                        // Add texture coordinates
                        vboData[offset++] = texCoordX; // S
                        vboData[offset++] = texCoordY; // T

                        texCoordX += txStep + (1/xLength);

                    }

                    texCoordY += txStep + (1/yLength);

                    texCoordX = 0.0f;
                }

                // Now build the index data
                final int nofStripsRequired = yLength - 1;
                final int nofDegensRequired = 2 * (nofStripsRequired - 1);
                final int verticesPerStrip = 2 * xLength;

                final short[] heightMapIndexData = new short[(verticesPerStrip * nofStripsRequired) + nofDegensRequired];

                offset = 0;

                for (int y = 0; y < yLength - 1; y++) {
                    if (y > 0) {
                        // Degenerate begin: repeat first vertex
                        heightMapIndexData[offset++] = (short) (y * yLength);
                    }

                    for (int x = 0; x < xLength; x++) {
                        // One part of the strip
                        heightMapIndexData[offset++] = (short) ((y * yLength) + x);
                        heightMapIndexData[offset++] = (short) (((y + 1) * yLength) + x);
                    }

                    if (y < yLength - 2) {
                        // Degenerate end: repeat last vertex
                        heightMapIndexData[offset++] = (short) (((y + 1) * yLength) + (xLength - 1));
                    }
                }

                indexCount = heightMapIndexData.length;

                final FloatBuffer vboDataBuffer = ByteBuffer
                        .allocateDirect(vboData.length * BYTES_PER_FLOAT)
                        .order(ByteOrder.nativeOrder())
                        .asFloatBuffer();
                vboDataBuffer.put(vboData).position(0);

                final ShortBuffer heightMapIndexDataBuffer = ByteBuffer
                        .allocateDirect(heightMapIndexData.length * BYTES_PER_SHORT)
                        .order(ByteOrder.nativeOrder())
                        .asShortBuffer();
                heightMapIndexDataBuffer.put(heightMapIndexData).position(0);

                textureCoordinates = ByteBuffer
                        .allocateDirect(textureCoordinateData.length * BYTES_PER_FLOAT)
                        .order(ByteOrder.nativeOrder())
                        .asFloatBuffer();
                textureCoordinates.put(textureCoordinateData).position(0);

                GLES20.glGenBuffers(1, vbo, 0);
                GLES20.glGenBuffers(1, ibo, 0);

                if (vbo[0] > 0 && ibo[0] > 0) {
                    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo[0]);
                    GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vboDataBuffer.capacity() * BYTES_PER_FLOAT,
                            vboDataBuffer, GLES20.GL_STATIC_DRAW);

                    GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
                    GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, heightMapIndexDataBuffer.capacity()
                            * BYTES_PER_SHORT, heightMapIndexDataBuffer, GLES20.GL_STATIC_DRAW);

                    // Why are these here?
                    GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
                    GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
                } else {
                    // errorHandler.handleError(ErrorHandler.ErrorType.BUFFER_CREATION_ERROR, "glGenBuffers");
                }
            } catch (Throwable t) {
                Log.w(TAG, t);
                // errorHandler.handleError(ErrorHandler.ErrorType.BUFFER_CREATION_ERROR, t.getLocalizedMessage());
            }
        }

        void render() {
            if (vbo[0] > 0 && ibo[0] > 0) {
                GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo[0]);

                // Bind Attributes
                GLES20.glVertexAttribPointer(positionAttribute, NOF_POS_ELEMENTS, GLES20.GL_FLOAT, false, STRIDE, 0);
                GLES20.glEnableVertexAttribArray(positionAttribute);

                GLES20.glVertexAttribPointer(normalAttribute, NOF_NORMAL_ELEMENTS, GLES20.GL_FLOAT, false, STRIDE,  NOF_POS_ELEMENTS * BYTES_PER_FLOAT);
                GLES20.glEnableVertexAttribArray(normalAttribute);

                GLES20.glVertexAttribPointer(colorAttribute, NOF_COLOR_ELEMENTS, GLES20.GL_FLOAT, false, STRIDE, (NOF_POS_ELEMENTS + NOF_NORMAL_ELEMENTS) * BYTES_PER_FLOAT);
                GLES20.glEnableVertexAttribArray(colorAttribute);

                // Texture
                int TEX_COORD_OFFSET = (NOF_POS_ELEMENTS + NOF_NORMAL_ELEMENTS + NOF_COLOR_ELEMENTS) * BYTES_PER_FLOAT;
                GLES20.glVertexAttribPointer(textureCoordinateAttribute, NOF_TEXTURE_COORD_ELEMENTS, GLES20.GL_FLOAT, false, STRIDE, TEX_COORD_OFFSET);
                GLES20.glEnableVertexAttribArray(textureCoordinateAttribute);

                // Draw
                GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
                GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, indexCount, GLES20.GL_UNSIGNED_SHORT, 0);

                GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
                GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
            }
        }

        void release() {
            if (vbo[0] > 0) {
                GLES20.glDeleteBuffers(vbo.length, vbo, 0);
                vbo[0] = 0;
            }

            if (ibo[0] > 0) {
                GLES20.glDeleteBuffers(ibo.length, ibo, 0);
                ibo[0] = 0;
            }
        }
    }
}