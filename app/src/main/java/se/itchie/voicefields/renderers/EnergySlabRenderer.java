package se.itchie.voicefields.renderers;

import android.opengl.GLES20;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class EnergySlabRenderer extends BaseRenderer {

    private SlabShape[] slabs = new SlabShape[32];
    private SlabShape threshold;

    double audioEnergy = 0; // Energy, normalized to fit into world space (0-31)
    double audioThreshold = 8; // Normalized to fit into world space (0-31) (will be overwritten)

    private boolean voiceDetected = false;

    private float backgroundR;
    private float backgroundG;
    private float backgroundB;

    private float[] originalBackgroundColors;

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {

        resetBackgroundColor();
        originalBackgroundColors = new float[]{backgroundR, backgroundG, backgroundB};

        // Set the background frame color
        GLES20.glClearColor(backgroundR, backgroundG, backgroundB, 1.0f);

        float slabSize;
        threshold = new SlabShape(2.0f, new float[]{0.6f, 0.232f, 0.1f, 0.5f});

        for (int i=0; i<slabs.length; i++) {
            float color[] = { 0.3f, (i+1)/33f, 0.22265f, 0.5f }; // purple to green
            slabSize = 8.0f / i;
            slabs[i] = new SlabShape(slabSize, color);
        }
    }

    @Override
    public void onDrawFrame(GL10 unused) {

        // Redraw background color
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        if(voiceDetected) {
            backgroundR = 0.31764f;
            backgroundG = 0.56254f;
            backgroundB = 0.31764f;
        } else {
            interpolateBackgroundColor();
        }

        GLES20.glClearColor(backgroundR, backgroundG, backgroundB, 1f);

        Matrix.setLookAtM(mViewMatrix,
                0,      // matrix offset
                0,      // eyeX
                0,      // eyeY
                -15,    // eyeZ (originally: 3)
                0f,     // centerX
                3.0f,   // centerY
                0f,     // centerZ
                0f,     // upX
                1.0f,   // upY
                0.0f);  // upZ

        int idx = 0;
        double audioEnergyDivided = audioEnergy + 1;
        for(SlabShape slab : slabs) {
            if (idx < audioEnergyDivided) {
                scratch = slab.update(idx, mViewMatrix, mProjectionMatrix);
                slab.draw(scratch);
            }

            idx++;
        }

        scratch = threshold.update(audioThreshold, mViewMatrix, mProjectionMatrix);
        threshold.draw(scratch);
    }

    private void interpolateBackgroundColor() {

        if(backgroundR > originalBackgroundColors[0] ) {
            backgroundR -= 0.004;
        }

        if(backgroundG > originalBackgroundColors[1]) {
            backgroundG -= 0.004;
        }

        if(backgroundB < originalBackgroundColors[2]) {
            backgroundB += 0.004;
        }
    }

    private void resetBackgroundColor() {
        backgroundR = 0.239f;
        backgroundG = 0.21568f;
        backgroundB = 0.560784f;
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);

        float ratio = (float) width / height;

        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 25);
    }

    public void setState(double[] state) {
        audioEnergy = Math.max(1, state[0]) * 2;
        audioThreshold = state[1] * 2;
        voiceDetected = state[2] == 1;
    }
}
